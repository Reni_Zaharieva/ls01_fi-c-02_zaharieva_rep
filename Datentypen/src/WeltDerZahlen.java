/**
  *   Aufgabe:  Recherechieren Sie im Internet !
  * 
  *   Sie d�rfen nicht die Namen der Variablen ver�ndern !!!
  *
  *   Vergessen Sie nicht den richtigen Datentyp !!
  *
  *
  * @version 1.0 from 30.09.2020
  * @author << Irena Zaharieva >>
  */

public class WeltDerZahlen {

  public static void main(String[] args) {
    
    /*  *********************************************************
    
         Zuerst werden die Variablen mit den Werten festgelegt!
    
    *********************************************************** */
    // Im Internet gefunden ?
    // Die Anzahl der Planeten in unserem Sonnensystem                    
    int anzahlPlaneten = 8;
    
    // Anzahl der Sterne in unserer Milchstra�e
    double anzahlSterne = 2E11;
    
    // Wie viele Einwohner hat Berlin?
    double bewohnerBerlin = 3.769E6;
    
    // Wie alt bist du?  Wie viele Tage sind das?
    int alterTage = 7543;
    
    // Wie viel wiegt das schwerste Tier der Welt?
    // Schreiben Sie das Gewicht in Kilogramm auf!
    double gewichtKilogramm =  1.9E5;
    
    // Schreiben Sie auf, wie viele km� das gr��te Land der Erde hat?
    double flaecheGroessteLand = 1.7E7;
    
    // Wie gro� ist das kleinste Land der Erde?
    
    float flaecheKleinsteLand = 0.44f; 
    
    
    
    
    /*  *********************************************************
    
         Programmieren Sie jetzt die Ausgaben der Werte in den Variablen
    
    *********************************************************** */
    
    System.out.printf("%S%d%n", "Anzhahl der Planeten: ", anzahlPlaneten);
    System.out.printf("%S%,.0f%n", "Anzahl der Sterne: ", anzahlSterne);
    System.out.printf("%S%,d%n", "Alter in Tagen: ", alterTage);
    System.out.printf("%S%,.0f%n", "Anzahl der Einwohner in Berlin: ", bewohnerBerlin);
    System.out.printf("%S%,.0f%s%n", "Gewicht des schwersten Tieres: ", gewichtKilogramm, " kg");
    System.out.printf("%S%,.0f%s%n", "Fl�che des gr��ten Landes: ", flaecheGroessteLand, " km�");
    System.out.printf("%S%.2f%s%n%n", "Fl�che des kleinsten Landes: ", flaecheKleinsteLand, " km�");
    
    
    
    
    System.out.println(" *******  Ende des Programms  ******* ");
    
  }
}

