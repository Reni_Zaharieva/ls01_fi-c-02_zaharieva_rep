/** Operatoren.java
   Uebung zu Operatoren in Java
   @author << Irena Zaharieva >>
   @version 1.0
*/
public class Operatoren {
  public static void main(String [] args){
    /* 1. Vereinbaren Sie zwei Ganzzahlen.*/
	  int x;
	  int y;

    System.out.println("UEBUNG ZU OPERATOREN IN JAVA\n");
    /* 2. Weisen Sie den Ganzzahlen die Werte 75 und 23 zu
          und geben Sie sie auf dem Bildschirm aus. */
    x = 75;
    y = 23;
    System.out.printf("%d%s%d%n", x, ", ", y);

    /* 3. Addieren Sie die Ganzzahlen
          und geben Sie das Ergebnis auf dem Bildschirm aus. */
    System.out.print("Addition: ");
    System.out.println(x + y);

    /* 4. Wenden Sie alle anderen arithmetischen Operatoren auf die
          Ganzzahlen an und geben Sie das Ergebnis jeweils auf dem
          Bildschirm aus. */
    System.out.print("Subtraktion: ");
    System.out.println(x - y);
    System.out.print("Multiplikation: ");
    System.out.println(x * y);
    System.out.print("Division: ");
    System.out.println(x / y);
    System.out.print("Modulo: ");
    System.out.println(x % y);

    /* 5. �berpr�fen Sie, ob die beiden Ganzzahlen gleich sind
          und geben Sie das Ergebnis auf dem Bildschirm aus. */
    if(x == y) 
    {
    	System.out.println("Die Zahlen sind gleich.");
    }
    else 
    {
    	System.out.println("Die Zahlen sind ungleich.");
    }

    /* 6. Wenden Sie drei anderen Vergleichsoperatoren auf die Ganzzahlen an
          und geben Sie das Ergebnis jeweils auf dem Bildschirm aus. */
    if(x > y)
    {
    	System.out.println("Die Zahl x ist gr��er als die Zahl y.");	
    } 
    else if(x < y) 	
    {
    	System.out.println("Die Zahl x ist kleiner als die Zahl y.");
    } 
    else if(x != y) 
    {
    	System.out.println("Die Zahlen sind einander nicht gleich.");
    }

    /* 7. �berpr�fen Sie, ob die beiden Ganzzahlen im  Interval [0;50] liegen
          und geben Sie das Ergebnis auf dem Bildschirm aus. */
    if(x > 0 && x < 50) 
    {
    	System.out.println("Die Zahl x liegt innerhalb des Intervalls [0;50].");
    }
    else
    {
    	System.out.println("Die Zahl x liegt nicht innerhalb des Intervalls [0;50].");
    }
    if(y > 0 && y < 50)
    {
    	System.out.println("Die Zahl y liegt innerhalb des Intervalls [0;50].");
    }
    else
    {
    	System.out.println("Die Zahl y liegt nicht innerhalb des Intervalls [0;50].");
    }
          
  }//main
}// Operatoren 