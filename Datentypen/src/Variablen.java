/** Variablen.java
    Erg�nzen Sie nach jedem Kommentar jeweils den Quellcode.
    @author << Irena Zaharieva >>
    @version 1.0
*/
public class Variablen {
  public static void main(String [] args){
    /* 1. Ein Z�hler soll die Programmdurchl�ufe z�hlen.
          Vereinbaren Sie eine geeignete Variable */
	  int programmdurchlaeufe;

    /* 2. Weisen Sie dem Z�hler den Wert 25 zu
          und geben Sie ihn auf dem Bildschirm aus.*/
	  programmdurchlaeufe = 25;
	  System.out.println("Anzahl der Programmdurchl�ufe: " + programmdurchlaeufe);

    /* 3. Durch die Eingabe eines Buchstabens soll der Men�punkt
          eines Programms ausgew�hlt werden.
          Vereinbaren Sie eine geeignete Variable */
	  char menue;

    /* 4. Weisen Sie dem Buchstaben den Wert 'C' zu
          und geben Sie ihn auf dem Bildschirm aus.*/
	  menue = 'C';
	  System.out.println("Um das Men� aufzurufen, dr�cken Sie: " + menue);
	  
    /* 5. F�r eine genaue astronomische Berechnung sind gro�e Zahlenwerte
          notwendig.
          Vereinbaren Sie eine geeignete Variable */
	  double astrom;

    /* 6. Weisen Sie der Zahl den Wert der Lichtgeschwindigkeit zu
          und geben Sie sie auf dem Bildschirm aus.*/
	  astrom = 299792458.0;
	  System.out.printf("%s%,.0f%s%n", "Lichtgeschiwindigkeit in Metern pro Sekunde: ", astrom, " m/s");

    /* 7. Sieben Personen gr�nden einen Verein. F�r eine Vereinsverwaltung
          soll die Anzahl der Mitglieder erfasst werden.
          Vereinbaren Sie eine geeignete Variable und initialisieren sie
          diese sinnvoll.*/
	  int anzahlMitglieder = 7;

    /* 8. Geben Sie die Anzahl der Mitglieder auf dem Bildschirm aus.*/
	  System.out.println("Anzahl der Vereinsmitglieder: " + anzahlMitglieder);

    /* 9. F�r eine Berechnung wird die elektrische Elementarladung ben�tigt.
          Vereinbaren Sie eine geeignete Variable und geben Sie sie auf
          dem Bildschirm aus.*/
	  final double ELEMENTARLADUNG = 1.602E-19;
	  System.out.printf("%s%s%n", "Kleinste frei existierende elektrische Ladungsmenge (Elementarladung): ", ""+ELEMENTARLADUNG+" As");

    /*10. Ein Buchhaltungsprogramm soll festhalten, ob eine Zahlung erfolgt ist.
          Vereinbaren Sie eine geeignete Variable. */
	  boolean istBezahlt;

    /*11. Die Zahlung ist erfolgt.
          Weisen Sie der Variable den entsprechenden Wert zu
          und geben Sie die Variable auf dem Bildschirm aus.*/
	  istBezahlt = true;
	  System.out.println("Zahlung ist erfolgt?: " + istBezahlt);

  }//main
}// Variablen