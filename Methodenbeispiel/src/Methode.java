public class Methode {

	public static void main(String[] args)
    {
		int zahl1 = 5;
		int zahl2 = 7;
		
		int erg = min(zahl1, zahl2);
		int erg2 = max(3, 6, 2);
		
		System.out.println("Ergebnis: " + erg2);
    }
	
	public static int min (int zahl1, int zahl2)
	{
		if (zahl1 < zahl2)
		{
			return zahl1;
		}
		else
		{
			return zahl2;
		}
	}
	
	public static int max (int x, int y, int z)
	{
		if ((x > y) && (x > z))
		{
			return x;
		}
		else if ((y > x) && (y > z))
		{
			return y;
		}
		else
		{
			return z;
		}
	}
}
