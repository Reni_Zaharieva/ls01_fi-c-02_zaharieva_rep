import java.util.Scanner;

public class Arraybeispiele {

	public static void main(String[] args) {
		
		Scanner tastatur = new Scanner(System.in);
		
		//Aufgabe 1
		System.out.println("Aufgabe 1:");
		int[] zahlen = new int[10];
		for(int i = 0; i < zahlen.length; i++)
		{
			zahlen[i] = i;
			System.out.print(zahlen[i] + " ");
		}
		System.out.println();
		
		
		//Aufgabe 2
		System.out.println("\nAufgabe 2:");
		int[] ungeradeZahlen = new int[20];
		int[] alleZahlen = new int[20];
		for(int i = 0; i < alleZahlen.length; i++)
		{
			alleZahlen[i] = i;
			if((i % 2) != 0)
			{
				ungeradeZahlen[i] = alleZahlen[i];
				System.out.print(ungeradeZahlen[i] + " ");
			}	
		}
		System.out.println();
		
		
		//Aufgabe 3
		System.out.println("\nAufgabe 3:");
		System.out.println("Bitte geben Sie ein fünfstelliges Wort ein: ");
		char[] palindrom = new char[5];
		String eingabe = tastatur.next();
		char[] zeichen = eingabe.toCharArray();
		for(int i = 0; i < palindrom.length; i++)
		{
			palindrom[i] = zeichen[i];
		}
		for(int i = palindrom.length - 1; i >= 0; i--)
		{
			System.out.print(palindrom[i]);
		}
		System.out.println();
		
		
		//Aufgabe 4a
		System.out.println("\nAufgabe 4:");
		int[] lottozahlen = new int[6];
		lottozahlen[0] = 3;		lottozahlen[3] = 18;
		lottozahlen[1] = 7;		lottozahlen[4] = 37;
		lottozahlen[2] = 12;	lottozahlen[5] = 42;
		int[] gerateneZahlen = new int[2];
		gerateneZahlen[0] = 12;		gerateneZahlen[1] = 13;
		boolean[] enthalten = new boolean[2];
		enthalten[0] = false;		enthalten[1] = false;
		
		System.out.print("[ ");
		for(int i = 0; i < lottozahlen.length; i++)
		{
			System.out.print(lottozahlen[i] + " ");
		}
		
		for(int i = 0; i < lottozahlen.length; i++)
		{
			if(gerateneZahlen[0] == lottozahlen[i])
			{
				enthalten[0] = true;
			}
			if(gerateneZahlen[1] == lottozahlen[i])
			{
				enthalten[1] = true;
			}
		}
		System.out.print("]\n");
		
		for(int i = 0; i < enthalten.length; i++)
		{
			if(enthalten[i] == true)
			{
				System.out.println("Die Zahl " + gerateneZahlen[i] + " ist in der Ziehung enthalten.");
			}
			if(enthalten[i] == false)
			{
				System.out.println("Die Zahl " + gerateneZahlen[i] + " ist nicht in der Ziehung enthalten.");
			}
		}	
		tastatur.close();
	}
}