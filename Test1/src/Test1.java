public class Test1 {

	public static void main(String[] args)
	{
		// Aufgabe 1
		System.out.printf("%s%n%n", "Aufgabe 1:");
		System.out.printf("   **   ");
		System.out.printf("\n*      *");
		System.out.printf("\n*      *");
		System.out.printf("\n   **   %n%n%n");
		
		// Aufgabe 2
		System.out.printf("%s%n%n", "Aufgabe 2:");
		System.out.printf("%-5s=%-19s=%4s%n", "0!", "", "1");
		System.out.printf("%-5s=%-19s=%4s%n", "1!", " 1", "1");
		System.out.printf("%-5s=%-19s=%4s%n", "2!", " 1 * 2", "2");
		System.out.printf("%-5s=%-19s=%4s%n", "3!", " 1 * 2 * 3", "6");
		System.out.printf("%-5s=%-19s=%4s%n", "4!", " 1 * 2 * 3 * 4", "24");		
		System.out.printf("%-5s=%-19s=%4s%n%n%n", "5!", " 1 * 2 * 3 * 4 * 5", "120");
		
		// Aufgabe 3
		System.out.printf("%s%n%n", "Aufgabe 3:");
		System.out.printf("%-12s|%10s%n", "Fahrenheit", "Celsius");
		System.out.printf("%22s%n", "-----------------------");
		System.out.printf("%-12d|%10.2f%n", -20, -28.8889);
		System.out.printf("%-12d|%10.2f%n", -10, -23.3333);
		System.out.printf("%-+12d|%10.2f%n", 0, -17.7778);
		System.out.printf("%-+12d|%10.2f%n", 20, -6.6667);
		System.out.printf("%-+12d|%10.2f%n", 30, -1.1111);				
	}
}
