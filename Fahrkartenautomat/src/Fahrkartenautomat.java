﻿import java.util.Scanner;

class Fahrkartenautomat
{
    public static void main(String[] args)
    {
       Scanner tastatur = new Scanner(System.in);
       double eingezahlterGesamtbetrag = 0.0;
       long millisekunde = 250;
       
       // Den zu zahlenden Betrag ermittelt der Automat
       // aufgrund der gewählten Fahrkarte(n).
       // -----------------------------------
       double gesamtbetrag = fahrkartenbestellungErfassen();							//START

       // Geldeinwurf
       // -----------
       while(eingezahlterGesamtbetrag < gesamtbetrag)									//genug Geld eingezahlt?
       {
    	   double eingezahlt = geldeinwurf(gesamtbetrag, eingezahlterGesamtbetrag);
    	   eingezahlterGesamtbetrag = eingezahlt;
       }
       gesamtbetrag = gesamtbetrag - eingezahlterGesamtbetrag;

       // Rückgeldberechnung und -Ausgabe
       // -------------------------------
       double betrag = rückgabeberechnung(eingezahlterGesamtbetrag, gesamtbetrag);
       muenzeAusgeben(betrag);
       
       // Fahrscheinausgabe und Wartezeit
       // -------------------------------
       fahrscheinausgabe(millisekunde);
       System.out.println("\n");

       // Schließt den Scanner, um Speicher zu sparen.
       tastatur.close();
    }
   
    //--------------------------------------------------------------------//METHODEN\\--------------------------------------------------------------------------\\
    
    // Fahrkartenbestellung
    // Hier werden die Fahrkarteninformationen in einer Methode verwaltet:
    // -------------------------------------------------------------------
    public static double fahrkartenbestellungErfassen()
    {
    	Scanner tastatur = new Scanner(System.in);
    	double lZuZahlenderBetrag = 0.0;
    	double lGesamtbetrag = 0.0;
    	int lAnzahlTickets = 0;
    	char lAntwort;
    	boolean lWiederholung = false;
    	boolean lKorrekteAntwort = false;
    	boolean lKorrekteFahrkarte = false;
    	
    	String[] fahrkartenbezeichnung = new String[10];
    	double[] fahrkartenpreis = new double[10];
    	fahrkartenbezeichnung[0] = "Einzelfahrschein Berlin AB";	fahrkartenbezeichnung[1] = "Einzelfahrschein Berlin BC";
    	fahrkartenbezeichnung[2] = "Einzelfahrschein Berlin ABC";	fahrkartenbezeichnung[3] = "Kurzstrecke";
    	fahrkartenbezeichnung[4] = "Tageskarte Berlin AB";			fahrkartenbezeichnung[5] = "Tageskarte Berlin BC";
    	fahrkartenbezeichnung[6] = "Tageskarte Berlin ABC";			fahrkartenbezeichnung[7] = "Kleingruppen-Tageskarte Berlin AB";
    	fahrkartenbezeichnung[8] = "Kleingruppen-Tageskarte Berlin BC";	fahrkartenbezeichnung[9] = "Kleingruppen-Tageskarte Berlin ABC";
    	fahrkartenpreis[0] = 2.90;	fahrkartenpreis[1] = 3.30; fahrkartenpreis[2] = 3.60; fahrkartenpreis[3] = 1.90;
    	fahrkartenpreis[4] = 8.60; fahrkartenpreis[5] = 9.00; fahrkartenpreis[6] = 9.60; fahrkartenpreis[7] = 23.50;
    	fahrkartenpreis[8] = 24.30; fahrkartenpreis[9] = 24.90;
    	
    	// 1.) Durch die Zusammenfassung der Fahrkarten und Preise in Arrays kann der Quellcode verkürzt werden, da die unterschiedlichen Optionen nicht einzeln,
    	// sondern alle zusammen per for-Schleife abgefragt werden können.
    	// 2.) Die Vorteile der neuen Implementierung gegenüber der alten sind, dass neue Fahrkarten viel simpler hinzuzufügen sind und es nicht länge nötig ist,
    	// für jede Fahrkarte eine eigene if-Verzweigung zu schreiben.
    	do
    	{
    		System.out.println("Wählen Sie ihre Wunschkarte für Berlin AB aus: ");														//START
    		for(int i = 0; i < fahrkartenbezeichnung.length; i++)
    		{
    			System.out.printf("\n%s%s%.2f%s%s%d%s", fahrkartenbezeichnung[i], " - ", fahrkartenpreis[i], "€", " (", i+1, ")");	//Ausgabe der Fahrkartenoptionen
    		}
    		System.out.println();
    		do																															//gültige Fahrkarte?
    		{
    			int lGewaehlteFahrkarte = tastatur.nextInt()-1;																			//Wahl des Fahrscheins
    			if(lGewaehlteFahrkarte < fahrkartenbezeichnung.length)
    			{
    				lZuZahlenderBetrag = fahrkartenpreis[lGewaehlteFahrkarte];
    				System.out.print("Anzahl der Tickets: ");
    				lAnzahlTickets = tastatur.nextInt();																				//Wahl Anzahl gewünschter Fahrkarten
    				lGesamtbetrag = lGesamtbetrag + (lZuZahlenderBetrag * lAnzahlTickets);												//Berechnung des Gesamtbetrages
    				lKorrekteFahrkarte = true;
    			}
    			else
    			{
    				System.out.println("Fehler, bitte geben Sie eine gültige Zahl an.");
    				lKorrekteFahrkarte = false;
    			}
    		}
    		while(lKorrekteFahrkarte == false);
    		System.out.println("\nMöchten Sie einen weiteren Tickenkauf tätigen? ");													//weiterer Ticketkauf?
    		do																															//gültige Eingabe?
    		{
    			lAntwort = tastatur.next().charAt(0);
    			if(lAntwort == 'j')
    			{
    				lWiederholung = true;
    				lKorrekteAntwort = true;
    			}
    			else if(lAntwort == 'n')
    			{
    				lWiederholung = false;
    				lKorrekteAntwort = true;
    			}
    			else
    			{
    				System.out.println("Fehler. Geben Sie bitte 'ja' oder 'nein' ein.");
    				lKorrekteAntwort = false;
    			}
    		}
    		while(lKorrekteAntwort == false);
    	}
    	while(lWiederholung == true);
    	return lGesamtbetrag;																											//Gesamtbetrag an Maincode weitergeben
    }
    
    // Geldeinwurf
    // Auf ungültige eingezahlte Beträge wird mit einer Fehlermeldung reagiert - eine erneute Eingabe muss anschließend stattfinden.
    // -----------------------------------------------------------------------------------------------------------------------------
	public static double geldeinwurf(double gesamtbetrag, double eingezahlterGesamtbetrag)
    {
       Scanner tastatur = new Scanner(System.in);
       double lEingeworfeneMünze = 0;
       
       System.out.printf("%s%.2f%s%n", "Noch zu zahlen: ", gesamtbetrag - eingezahlterGesamtbetrag, "€");
 	   System.out.print("Eingabe (mind. 5Ct): ");
 	   lEingeworfeneMünze = tastatur.nextDouble();																						//Eingabe des gezahlzen Geldes
 	   if(lEingeworfeneMünze < 0.05)																									//unter 5 Cent?
 	   {
 		   System.out.println("\nFehler. Dieser Automat nimmt nur Geldbeträge von 5 CENT oder mehr.\n");
 	   }
 	   else
 	   {
 		   eingezahlterGesamtbetrag += lEingeworfeneMünze;
 	   }
       
       return eingezahlterGesamtbetrag;																									//insgesamt eingezahltes Geld an Maincode weitergeben
    }
	
	// Rückgeldberechnung
    // ------------------
    public static double rückgabeberechnung(double eingezahlterGesamtbetrag, double gesamtbetrag)
    {
    	double lRückgabebetrag;
    	lRückgabebetrag = gesamtbetrag*-1;																								//Rückgeldberechnung
    	return lRückgabebetrag;
    }
    
    //Münz- und Scheineausgabe
    //------------------------
    public static void muenzeAusgeben(double betrag) 
    { 
    	String[] lEinheit = new String[8];
    	lEinheit[0] = "EURO"; lEinheit[1] = "EURO"; lEinheit[2] = "CENT"; lEinheit[3] = "CENT"; lEinheit[4] = "CENT"; lEinheit[5] = "CENT"; lEinheit[6] = "EURO";
    	
    	//Münzen
    	String[] muenzausgabe = new String[7];
    	muenzausgabe[0] = "   *  *  *   "; muenzausgabe[1] = " *         * "; muenzausgabe[2] = "*     "; muenzausgabe[3] = "     *"; muenzausgabe[4] = "*    "; muenzausgabe[5] = "   *"; muenzausgabe[6] = "    *";
    	int[] lMuenzwert = new int[7];
    	lMuenzwert[0] = 0; lMuenzwert[1] = 1; lMuenzwert[2] = 50; lMuenzwert[3] = 20; lMuenzwert[4] = 10; lMuenzwert[5] = 5; lMuenzwert[6] = 2; //[0]=fehlerdodge, [1]=1€, [2]=50ct, [3]=20ct, [4]=10ct, [5]=5ct, [6]=2€;
    	int[] lZifferanzahl = new int[7];
    	lZifferanzahl[0] = 1; lZifferanzahl[1] = 1; lZifferanzahl[2] = 2; lZifferanzahl[3] = 2; lZifferanzahl[4] = 2; lZifferanzahl[5] = 1; lZifferanzahl[6] = 1;
    	int[] lAnzahlMuenzen = new int[7];
    	int lMuenzenGesamt = 0;
    	int lErsteMuenze = 0;
 	    int lZweiteMuenze = 0;
 	    int lDritteMuenze = 0;
 	    int lVierteMuenze = 0;
 	    int lFuenfteMuenze = 0;
 	    int lSechsteMuenze = 0;
 	    
 	    //Scheine
 	    String[] scheineausgabe = new String[6];
	    scheineausgabe[0] = "* * * * * * * * * * * * * *  "; scheineausgabe[1] = "*                         *  "; scheineausgabe[2] = "*            "; scheineausgabe[3] = "            *  "; scheineausgabe[4] = "*           EURO          *  "; scheineausgabe[5] = "           *  ";
	    int[] lScheinewert = new int[4];
    	lScheinewert[0] = 0; lScheinewert[1] = 5; lScheinewert[2] = 10; lScheinewert[3] = 20; //[0]=fehlerdodge, [1]=5€, [2]=10€, [3]=20€;
    	int[] lAnzahlScheine = new int[4];
    	int lScheineGesamt = 0;
    	int lErsterSchein = 0;
 	    int lZweiterSchein = 0;
 	    int lDritterSchein = 0;
 	    
    	if(betrag > 0.0)																											 //Rückgeld vorhanden?
        {
     	   betrag = Math.round(betrag*1000)/1000.0;
    	   System.out.printf("%s%.2f%s", "\nDer Rückgabebetrag in Höhe von ", betrag, " EURO");
     	   System.out.println(" wird in folgenden Münzen ausgezahlt:\n");

     	    while(betrag >= 20.0) // 20 EURO-Scheine																				 //welche Münzen?: prüft, ob x-viel Geld zurückzuzahlen ist
            {
          	   betrag -= 20.0;																										 //subtrahiert Wert vom zurückzuzahlenden Betrag
          	   lAnzahlScheine[3]++;																								     //füllt Array an entsprechender Stelle
            }
     	   betrag = Math.round(betrag*1000)/1000.0;
     	    while(betrag >= 10.0) // 10 EURO-Scheine
            {
         	   betrag -= 10.0;
         	   lAnzahlScheine[2]++;
            }
     	   betrag = Math.round(betrag*1000)/1000.0;
     	    while(betrag >= 5.0) // 5 EURO-Scheine
            {
        	   betrag -= 5.0;
        	   lAnzahlScheine[1]++;
            }
     	   betrag = Math.round(betrag*1000)/1000.0;
     	    while(betrag >= 2.0) // 2 EURO-Münzen
            {
            	betrag -= 2.0;
            	lAnzahlMuenzen[6]++;
            }
     	   betrag = Math.round(betrag*1000)/1000.0;
            while(betrag >= 1.0) // 1 EURO-Münzen
            {
            	betrag -= 1.0;
         		lAnzahlMuenzen[1]++;
            }
            betrag = Math.round(betrag*1000)/1000.0;
            while(betrag >= 0.5) // 50 CENT-Münzen
            {
            	betrag -= 0.5;
            	lAnzahlMuenzen[2]++;
            }
            betrag = Math.round(betrag*1000)/1000.0;
            while(betrag >= 0.2) // 20 CENT-Münzen
            {
            	betrag -= 0.2;
            	lAnzahlMuenzen[3]++;
            }
            betrag = Math.round(betrag*1000)/1000.0;
            while(betrag >= 0.1) // 10 CENT-Münzen
            {
            	betrag -= 0.1;
            	lAnzahlMuenzen[4]++;
            }
            betrag = Math.round(betrag*1000)/1000.0;
            while(betrag >= 0.05)// 5 CENT-Münzen
            {
            	betrag -= 0.05;
            	lAnzahlMuenzen[5]++;
            }
            lScheineGesamt = lAnzahlScheine[0] + lAnzahlScheine[1] + lAnzahlScheine[2] + lAnzahlScheine[3];											//Zusammenzählung aller Münzen
            lMuenzenGesamt = lAnzahlMuenzen[0] + lAnzahlMuenzen[1] + lAnzahlMuenzen[2] + lAnzahlMuenzen[3] + lAnzahlMuenzen[4] + lAnzahlMuenzen[5] + lAnzahlMuenzen[6];
            //System.out.println(lMuenzenGesamt);
            
            if(lScheineGesamt == 1)																								//wie viele Münzen?: guckt, wie viele Münzen insgesamt auszugeben sind
            {
            	for(int i = 0; i < lAnzahlScheine.length; i++)																	//welche Münzen?: guckt, welche Stellen im Array gefüllt sind (welche Münzen auszugeben sind)
            	{
            		if(lAnzahlScheine[i] != 0)
            		{
            			if(i == 1)
            			{
            				System.out.printf("%s%n%s%n%s%d%s%n%s%n%s%n%s\n", scheineausgabe[0], scheineausgabe[1], scheineausgabe[2], lScheinewert[i], scheineausgabe[3], scheineausgabe[4], scheineausgabe[1], scheineausgabe[0]);
            			}
            			else if(i > 1)
            			{
            				System.out.printf("%s%n%s%n%s%d%s%n%s%n%s%n%s\n", scheineausgabe[0], scheineausgabe[1], scheineausgabe[2], lScheinewert[i], scheineausgabe[5], scheineausgabe[4], scheineausgabe[1], scheineausgabe[0]);
            			}
            		}
            	}
            }
            if(lScheineGesamt == 2)
            {
            	for(int i = 0, j = 0; i < lAnzahlScheine.length; i++, j++)
            	{
            		if(lAnzahlScheine[i] != 0)
            		{
            			if(lErsterSchein == 0)
            			{
            				lErsterSchein = i;																					//speichert auszugebende Münze in neuer Variable
            				lAnzahlScheine[i]--;																				//subtrahiert 1 von Stelle im Array, hackt entsprechende Münze quasi als ausgegeben ab
            			}
            		}
            		if(lAnzahlScheine[j] != 0)
            		{
            			if(lZweiterSchein == 0)
            			{
            				lZweiterSchein = j;
            				lAnzahlScheine[j]--;
            			}
            		}
            	}
            	if((lErsterSchein == 1) && (lZweiterSchein == 1))																//Ausgabe entsprechender Münzen, die in den neuen Variablen gespeichert worden sind
            	{ 
            		System.out.printf("%s%s%n%s%s%n%s%d%s%s%d%s%n%s%s%n%s%s%n%s%s\n", scheineausgabe[0], scheineausgabe[0], scheineausgabe[1], scheineausgabe[1], scheineausgabe[2], lScheinewert[lErsterSchein], scheineausgabe[3], scheineausgabe[2], lScheinewert[lZweiterSchein], scheineausgabe[3], scheineausgabe[4], scheineausgabe[4], scheineausgabe[1], scheineausgabe[1], scheineausgabe[0], scheineausgabe[0]);
            	}
            	else if((lErsterSchein == 1) && (lZweiterSchein > 1))
            	{
            		System.out.printf("%s%s%n%s%s%n%s%d%s%s%d%s%n%s%s%n%s%s%n%s%s\n", scheineausgabe[0], scheineausgabe[0], scheineausgabe[1], scheineausgabe[1], scheineausgabe[2], lScheinewert[lErsterSchein], scheineausgabe[3], scheineausgabe[2], lScheinewert[lZweiterSchein], scheineausgabe[5], scheineausgabe[4], scheineausgabe[4], scheineausgabe[1], scheineausgabe[1], scheineausgabe[0], scheineausgabe[0]);
            	}
            	else if((lErsterSchein > 1) && (lZweiterSchein == 1))
            	{
            		System.out.printf("%s%s%n%s%s%n%s%d%s%s%d%s%n%s%s%n%s%s%n%s%s\n", scheineausgabe[0], scheineausgabe[0], scheineausgabe[1], scheineausgabe[1], scheineausgabe[2], lScheinewert[lErsterSchein], scheineausgabe[5], scheineausgabe[2], lScheinewert[lZweiterSchein], scheineausgabe[3], scheineausgabe[4], scheineausgabe[4], scheineausgabe[1], scheineausgabe[1], scheineausgabe[0], scheineausgabe[0]);
            	}
            	else if((lErsterSchein > 1) && (lZweiterSchein > 1))
            	{
            		System.out.printf("%s%s%n%s%s%n%s%d%s%s%d%s%n%s%s%n%s%s%n%s%s\n", scheineausgabe[0], scheineausgabe[0], scheineausgabe[1], scheineausgabe[1], scheineausgabe[2], lScheinewert[lErsterSchein], scheineausgabe[5], scheineausgabe[2], lScheinewert[lZweiterSchein], scheineausgabe[5], scheineausgabe[4], scheineausgabe[4], scheineausgabe[1], scheineausgabe[1], scheineausgabe[0], scheineausgabe[0]);
            	}
            }
            if((lScheineGesamt == 3) || (lScheineGesamt > 3))
            {
            	for(int i = 0, j = 0, k = 0; i < lAnzahlScheine.length; i++, j++, k++)
            	{
            		if(lAnzahlScheine[i] != 0)
            		{
            			if(lErsterSchein == 0)
            			{
            				lErsterSchein = i;
            				lAnzahlScheine[i]--;
            			}
            		}
            		if(lAnzahlScheine[j] != 0)
            		{
            			if(lZweiterSchein == 0)
            			{
            				lZweiterSchein = j;
            				lAnzahlScheine[j]--;
            			}
            		}
            		if(lAnzahlScheine[k] != 0)
            		{
            			if(lDritterSchein == 0)
            			{
            				lDritterSchein = k;
            				lAnzahlScheine[k]--;
            			}
            		}
            	}
            	if((lErsterSchein == 1) && (lZweiterSchein == 1) && (lDritterSchein == 1))
            	{
            		System.out.printf("%s%s%s%n%s%s%s%n%s%d%s%s%d%s%s%d%s%n%s%s%s%n%s%s%s%n%s%s%s\n", scheineausgabe[0], scheineausgabe[0], scheineausgabe[0], scheineausgabe[1], scheineausgabe[1], scheineausgabe[1], scheineausgabe[2], lScheinewert[lErsterSchein], scheineausgabe[3], scheineausgabe[2], lScheinewert[lZweiterSchein], scheineausgabe[3], scheineausgabe[2], lScheinewert[lDritterSchein], scheineausgabe[3], scheineausgabe[4], scheineausgabe[4], scheineausgabe[4], scheineausgabe[1], scheineausgabe[1], scheineausgabe[1], scheineausgabe[0], scheineausgabe[0], scheineausgabe[0]);
            	}
            	else if((lErsterSchein > 1) && (lZweiterSchein == 1) && (lDritterSchein > 1))
            	{
            		System.out.printf("%s%s%s%n%s%s%s%n%s%d%s%s%d%s%s%d%s%n%s%s%s%n%s%s%s%n%s%s%s\n", scheineausgabe[0], scheineausgabe[0], scheineausgabe[0], scheineausgabe[1], scheineausgabe[1], scheineausgabe[1], scheineausgabe[2], lScheinewert[lErsterSchein], scheineausgabe[5], scheineausgabe[2], lScheinewert[lZweiterSchein], scheineausgabe[3], scheineausgabe[2], lScheinewert[lDritterSchein], scheineausgabe[3], scheineausgabe[4], scheineausgabe[4], scheineausgabe[4], scheineausgabe[1], scheineausgabe[1], scheineausgabe[1], scheineausgabe[0], scheineausgabe[0], scheineausgabe[0]);
            	}
            	else if((lErsterSchein == 1) && (lZweiterSchein > 1) && (lDritterSchein == 1))
            	{
            		System.out.printf("%s%s%s%n%s%s%s%n%s%d%s%s%d%s%s%d%s%n%s%s%s%n%s%s%s%n%s%s%s\n", scheineausgabe[0], scheineausgabe[0], scheineausgabe[0], scheineausgabe[1], scheineausgabe[1], scheineausgabe[1], scheineausgabe[2], lScheinewert[lErsterSchein], scheineausgabe[3], scheineausgabe[2], lScheinewert[lZweiterSchein], scheineausgabe[5], scheineausgabe[2], lScheinewert[lDritterSchein], scheineausgabe[3], scheineausgabe[4], scheineausgabe[4], scheineausgabe[4], scheineausgabe[1], scheineausgabe[1], scheineausgabe[1], scheineausgabe[0], scheineausgabe[0], scheineausgabe[0]);
            	}
            	else if((lErsterSchein == 1) && (lZweiterSchein == 1) && (lDritterSchein > 1))
            	{
            		System.out.printf("%s%s%s%n%s%s%s%n%s%d%s%s%d%s%s%d%s%n%s%s%s%n%s%s%s%n%s%s%s\n", scheineausgabe[0], scheineausgabe[0], scheineausgabe[0], scheineausgabe[1], scheineausgabe[1], scheineausgabe[1], scheineausgabe[2], lScheinewert[lErsterSchein], scheineausgabe[3], scheineausgabe[2], lScheinewert[lZweiterSchein], scheineausgabe[3], scheineausgabe[2], lScheinewert[lDritterSchein], scheineausgabe[5], scheineausgabe[4], scheineausgabe[4], scheineausgabe[4], scheineausgabe[1], scheineausgabe[1], scheineausgabe[1], scheineausgabe[0], scheineausgabe[0], scheineausgabe[0]);
            	}
            	else if((lErsterSchein > 1) && (lZweiterSchein > 1) && (lDritterSchein == 1))
            	{
            		System.out.printf("%s%s%s%n%s%s%s%n%s%d%s%s%d%s%s%d%s%n%s%s%s%n%s%s%s%n%s%s%s\n", scheineausgabe[0], scheineausgabe[0], scheineausgabe[0], scheineausgabe[1], scheineausgabe[1], scheineausgabe[1], scheineausgabe[2], lScheinewert[lErsterSchein], scheineausgabe[5], scheineausgabe[2], lScheinewert[lZweiterSchein], scheineausgabe[5], scheineausgabe[2], lScheinewert[lDritterSchein], scheineausgabe[3], scheineausgabe[4], scheineausgabe[4], scheineausgabe[4], scheineausgabe[1], scheineausgabe[1], scheineausgabe[1], scheineausgabe[0], scheineausgabe[0], scheineausgabe[0]);
            	}
            	else if((lErsterSchein == 1) && (lZweiterSchein > 1) && (lDritterSchein > 1))
            	{
            		System.out.printf("%s%s%s%n%s%s%s%n%s%d%s%s%d%s%s%d%s%n%s%s%s%n%s%s%s%n%s%s%s\n", scheineausgabe[0], scheineausgabe[0], scheineausgabe[0], scheineausgabe[1], scheineausgabe[1], scheineausgabe[1], scheineausgabe[2], lScheinewert[lErsterSchein], scheineausgabe[3], scheineausgabe[2], lScheinewert[lZweiterSchein], scheineausgabe[5], scheineausgabe[2], lScheinewert[lDritterSchein], scheineausgabe[5], scheineausgabe[4], scheineausgabe[4], scheineausgabe[4], scheineausgabe[1], scheineausgabe[1], scheineausgabe[1], scheineausgabe[0], scheineausgabe[0], scheineausgabe[0]);
            	}
            	else if((lErsterSchein > 1) && (lZweiterSchein == 1) && (lDritterSchein > 1))
            	{
            		System.out.printf("%s%s%s%n%s%s%s%n%s%d%s%s%d%s%s%d%s%n%s%s%s%n%s%s%s%n%s%s%s\n", scheineausgabe[0], scheineausgabe[0], scheineausgabe[0], scheineausgabe[1], scheineausgabe[1], scheineausgabe[1], scheineausgabe[2], lScheinewert[lErsterSchein], scheineausgabe[5], scheineausgabe[2], lScheinewert[lZweiterSchein], scheineausgabe[3], scheineausgabe[2], lScheinewert[lDritterSchein], scheineausgabe[5], scheineausgabe[4], scheineausgabe[4], scheineausgabe[4], scheineausgabe[1], scheineausgabe[1], scheineausgabe[1], scheineausgabe[0], scheineausgabe[0], scheineausgabe[0]);
            	}
            	else if((lErsterSchein > 1) && (lZweiterSchein > 1) && (lDritterSchein > 1))
            	{
            		System.out.printf("%s%s%s%n%s%s%s%n%s%d%s%s%d%s%s%d%s%n%s%s%s%n%s%s%s%n%s%s%s\n", scheineausgabe[0], scheineausgabe[0], scheineausgabe[0], scheineausgabe[1], scheineausgabe[1], scheineausgabe[1], scheineausgabe[2], lScheinewert[lErsterSchein], scheineausgabe[5], scheineausgabe[2], lScheinewert[lZweiterSchein], scheineausgabe[5], scheineausgabe[2], lScheinewert[lDritterSchein], scheineausgabe[5], scheineausgabe[4], scheineausgabe[4], scheineausgabe[4], scheineausgabe[1], scheineausgabe[1], scheineausgabe[1], scheineausgabe[0], scheineausgabe[0], scheineausgabe[0]);
            	}
            }
            if(lScheineGesamt == 4)																								//wie viele Münzen?: guckt, wie viele Münzen insgesamt auszugeben sind
            {
            	for(int l = 0; l < lAnzahlScheine.length; l++)																	//welche Münzen?: guckt, welche Stellen im Array gefüllt sind (welche Münzen auszugeben sind)
            	{
            		if(lAnzahlScheine[l] != 0)
            		{
            			if(l == 1)
            			{
            				System.out.printf("%s%n%s%n%s%d%s%n%s%n%s%n%s\n", scheineausgabe[0], scheineausgabe[1], scheineausgabe[2], lScheinewert[l], scheineausgabe[3], scheineausgabe[4], scheineausgabe[1], scheineausgabe[0]);
            			}
            			else if(l > 1)
            			{
            				System.out.printf("%s%n%s%n%s%d%s%n%s%n%s%n%s\n", scheineausgabe[0], scheineausgabe[1], scheineausgabe[2], lScheinewert[l], scheineausgabe[5], scheineausgabe[4], scheineausgabe[1], scheineausgabe[0]);
            			}
            		}
            	}
            }
            
            if(lMuenzenGesamt == 1)
            {
            	for(int i = 0; i < lAnzahlMuenzen.length; i++)
            	{
            		if(lAnzahlMuenzen[i] != 0)
            		{
            			if(lZifferanzahl[i] == 1)
            			{
            				System.out.printf("%s%n%s%n%s%d%s%n%s%s%s%n%s%n%s\n", muenzausgabe[0], muenzausgabe[1], muenzausgabe[2], lMuenzwert[i], muenzausgabe[3], muenzausgabe[4], lEinheit[i], muenzausgabe[5], muenzausgabe[1], muenzausgabe[0]);
            			}
            			else
            			{
            				System.out.printf("%s%n%s%n%s%d%s%n%s%s%s%n%s%n%s\n", muenzausgabe[0], muenzausgabe[1], muenzausgabe[2], lMuenzwert[i], muenzausgabe[6], muenzausgabe[4], lEinheit[i], muenzausgabe[5], muenzausgabe[1], muenzausgabe[0]);
            			}
            			lAnzahlMuenzen[i]--;
            		}
            	}
            }
            if(lMuenzenGesamt == 2)
            {
            	for(int i = 0, j = 0; i < lAnzahlMuenzen.length; i++, j++)
            	{
            		if(lAnzahlMuenzen[i] != 0)
            		{
            			if(lErsteMuenze == 0)
            			{
            				lErsteMuenze = i;
            				lAnzahlMuenzen[i]--;
            			}
            		}
            		if(lAnzahlMuenzen[j] != 0)
            		{
            			if(lZweiteMuenze == 0)
            			{
            				lZweiteMuenze = j;
            				lAnzahlMuenzen[j]--;
            			}
            		}
            	}
            	if((lZifferanzahl[lErsteMuenze] == 1) && (lZifferanzahl[lZweiteMuenze] == 1))
            	{
            		System.out.printf("%s%s%n%s%s%n%s%d%s%s%d%s%n%s%s%s%s%s%s%n%s%s%n%s%s\n", muenzausgabe[0], muenzausgabe[0], muenzausgabe[1], muenzausgabe[1], muenzausgabe[2], lMuenzwert[lErsteMuenze], muenzausgabe[3], muenzausgabe[2], lMuenzwert[lZweiteMuenze], muenzausgabe[3], muenzausgabe[4], lEinheit[lErsteMuenze], muenzausgabe[5], muenzausgabe[4], lEinheit[lZweiteMuenze], muenzausgabe[5], muenzausgabe[1], muenzausgabe[1], muenzausgabe[0], muenzausgabe[0]);
            	}
            	else if((lZifferanzahl[lErsteMuenze] == 1) && (lZifferanzahl[lZweiteMuenze] == 2))
            	{
            		System.out.printf("%s%s%n%s%s%n%s%d%s%s%d%s%n%s%s%s%s%s%s%n%s%s%n%s%s\n", muenzausgabe[0], muenzausgabe[0], muenzausgabe[1], muenzausgabe[1], muenzausgabe[2], lMuenzwert[lErsteMuenze], muenzausgabe[3], muenzausgabe[2], lMuenzwert[lZweiteMuenze], muenzausgabe[6], muenzausgabe[4], lEinheit[lErsteMuenze], muenzausgabe[5], muenzausgabe[4], lEinheit[lZweiteMuenze], muenzausgabe[5], muenzausgabe[1], muenzausgabe[1], muenzausgabe[0], muenzausgabe[0]);
            	}
            	else if((lZifferanzahl[lErsteMuenze] == 2) && (lZifferanzahl[lZweiteMuenze] == 1))
            	{
            		System.out.printf("%s%s%n%s%s%n%s%d%s%s%d%s%n%s%s%s%s%s%s%n%s%s%n%s%s\n", muenzausgabe[0], muenzausgabe[0], muenzausgabe[1], muenzausgabe[1], muenzausgabe[2], lMuenzwert[lErsteMuenze], muenzausgabe[6], muenzausgabe[2], lMuenzwert[lZweiteMuenze], muenzausgabe[3], muenzausgabe[4], lEinheit[lErsteMuenze], muenzausgabe[5], muenzausgabe[4], lEinheit[lZweiteMuenze], muenzausgabe[5], muenzausgabe[1], muenzausgabe[1], muenzausgabe[0], muenzausgabe[0]);
            	}
            	else if((lZifferanzahl[lErsteMuenze] == 2) && (lZifferanzahl[lZweiteMuenze] == 2))
            	{
            		System.out.printf("%s%s%n%s%s%n%s%d%s%s%d%s%n%s%s%s%s%s%s%n%s%s%n%s%s\n", muenzausgabe[0], muenzausgabe[0], muenzausgabe[1], muenzausgabe[1], muenzausgabe[2], lMuenzwert[lErsteMuenze], muenzausgabe[6], muenzausgabe[2], lMuenzwert[lZweiteMuenze], muenzausgabe[6], muenzausgabe[4], lEinheit[lErsteMuenze], muenzausgabe[5], muenzausgabe[4], lEinheit[lZweiteMuenze], muenzausgabe[5], muenzausgabe[1], muenzausgabe[1], muenzausgabe[0], muenzausgabe[0]);
            	}
            }
            if((lMuenzenGesamt == 3) || (lMuenzenGesamt > 3))
            {
            	for(int i = 0, j = 0, k = 0; i < lAnzahlMuenzen.length; i++, j++, k++)
            	{
            		if(lAnzahlMuenzen[i] != 0)
            		{
            			if(lErsteMuenze == 0)
            			{
            				lErsteMuenze = i;
            				lAnzahlMuenzen[i]--;
            			}
            		}
            		if(lAnzahlMuenzen[j] != 0)
            		{
            			if(lZweiteMuenze == 0)
            			{
            				lZweiteMuenze = j;
            				lAnzahlMuenzen[j]--;
            			}
            		}
            		if(lAnzahlMuenzen[k] != 0)
            		{
            			if(lDritteMuenze == 0)
            			{
            				lDritteMuenze = k;
            				lAnzahlMuenzen[k]--;
            			}
            		}
            	}
            	if((lZifferanzahl[lErsteMuenze] == 1) && (lZifferanzahl[lZweiteMuenze] == 1) && (lZifferanzahl[lDritteMuenze] == 1))
            	{
            		System.out.printf("%s%s%s%n%s%s%s%n%s%d%s%s%d%s%s%d%s%n%s%s%s%s%s%s%s%s%s%n%s%s%s%n%s%s%s\n", muenzausgabe[0], muenzausgabe[0], muenzausgabe[0], muenzausgabe[1], muenzausgabe[1], muenzausgabe[1], muenzausgabe[2], lMuenzwert[lErsteMuenze], muenzausgabe[3], muenzausgabe[2], lMuenzwert[lZweiteMuenze], muenzausgabe[3], muenzausgabe[2], lMuenzwert[lDritteMuenze], muenzausgabe[3], muenzausgabe[4], lEinheit[lErsteMuenze], muenzausgabe[5], muenzausgabe[4], lEinheit[lZweiteMuenze], muenzausgabe[5], muenzausgabe[4], lEinheit[lDritteMuenze], muenzausgabe[5], muenzausgabe[1], muenzausgabe[1], muenzausgabe[1], muenzausgabe[0], muenzausgabe[0], muenzausgabe[0]);
            	}
            	else if((lZifferanzahl[lErsteMuenze] == 1) && (lZifferanzahl[lZweiteMuenze] == 1) && (lZifferanzahl[lDritteMuenze] == 2))
            	{
            		System.out.printf("%s%s%s%n%s%s%s%n%s%d%s%s%d%s%s%d%s%n%s%s%s%s%s%s%s%s%s%n%s%s%s%n%s%s%s\n", muenzausgabe[0], muenzausgabe[0], muenzausgabe[0], muenzausgabe[1], muenzausgabe[1], muenzausgabe[1], muenzausgabe[2], lMuenzwert[lErsteMuenze], muenzausgabe[3], muenzausgabe[2], lMuenzwert[lZweiteMuenze], muenzausgabe[3], muenzausgabe[2], lMuenzwert[lDritteMuenze], muenzausgabe[6], muenzausgabe[4], lEinheit[lErsteMuenze], muenzausgabe[5], muenzausgabe[4], lEinheit[lZweiteMuenze], muenzausgabe[5], muenzausgabe[4], lEinheit[lDritteMuenze], muenzausgabe[5], muenzausgabe[1], muenzausgabe[1], muenzausgabe[1], muenzausgabe[0], muenzausgabe[0], muenzausgabe[0]);
            	}
            	else if((lZifferanzahl[lErsteMuenze] == 1) && (lZifferanzahl[lZweiteMuenze] == 2) && (lZifferanzahl[lDritteMuenze] == 1))
            	{
            		System.out.printf("%s%s%s%n%s%s%s%n%s%d%s%s%d%s%s%d%s%n%s%s%s%s%s%s%s%s%s%n%s%s%s%n%s%s%s\n", muenzausgabe[0], muenzausgabe[0], muenzausgabe[0], muenzausgabe[1], muenzausgabe[1], muenzausgabe[1], muenzausgabe[2], lMuenzwert[lErsteMuenze], muenzausgabe[3], muenzausgabe[2], lMuenzwert[lZweiteMuenze], muenzausgabe[6], muenzausgabe[2], lMuenzwert[lDritteMuenze], muenzausgabe[3], muenzausgabe[4], lEinheit[lErsteMuenze], muenzausgabe[5], muenzausgabe[4], lEinheit[lZweiteMuenze], muenzausgabe[5], muenzausgabe[4], lEinheit[lDritteMuenze], muenzausgabe[5], muenzausgabe[1], muenzausgabe[1], muenzausgabe[1], muenzausgabe[0], muenzausgabe[0], muenzausgabe[0]);
            	}
            	else if((lZifferanzahl[lErsteMuenze] == 2) && (lZifferanzahl[lZweiteMuenze] == 1) && (lZifferanzahl[lDritteMuenze] == 1))
            	{
            		System.out.printf("%s%s%s%n%s%s%s%n%s%d%s%s%d%s%s%d%s%n%s%s%s%s%s%s%s%s%s%n%s%s%s%n%s%s%s\n", muenzausgabe[0], muenzausgabe[0], muenzausgabe[0], muenzausgabe[1], muenzausgabe[1], muenzausgabe[1], muenzausgabe[2], lMuenzwert[lErsteMuenze], muenzausgabe[6], muenzausgabe[2], lMuenzwert[lZweiteMuenze], muenzausgabe[3], muenzausgabe[2], lMuenzwert[lDritteMuenze], muenzausgabe[3], muenzausgabe[4], lEinheit[lErsteMuenze], muenzausgabe[5], muenzausgabe[4], lEinheit[lZweiteMuenze], muenzausgabe[5], muenzausgabe[4], lEinheit[lDritteMuenze], muenzausgabe[5], muenzausgabe[1], muenzausgabe[1], muenzausgabe[1], muenzausgabe[0], muenzausgabe[0], muenzausgabe[0]);
            	}
            	else if((lZifferanzahl[lErsteMuenze] == 2) && (lZifferanzahl[lZweiteMuenze] == 2) && (lZifferanzahl[lDritteMuenze] == 1))
            	{
            		System.out.printf("%s%s%s%n%s%s%s%n%s%d%s%s%d%s%s%d%s%n%s%s%s%s%s%s%s%s%s%n%s%s%s%n%s%s%s\n", muenzausgabe[0], muenzausgabe[0], muenzausgabe[0], muenzausgabe[1], muenzausgabe[1], muenzausgabe[1], muenzausgabe[2], lMuenzwert[lErsteMuenze], muenzausgabe[6], muenzausgabe[2], lMuenzwert[lZweiteMuenze], muenzausgabe[6], muenzausgabe[2], lMuenzwert[lDritteMuenze], muenzausgabe[3], muenzausgabe[4], lEinheit[lErsteMuenze], muenzausgabe[5], muenzausgabe[4], lEinheit[lZweiteMuenze], muenzausgabe[5], muenzausgabe[4], lEinheit[lDritteMuenze], muenzausgabe[5], muenzausgabe[1], muenzausgabe[1], muenzausgabe[1], muenzausgabe[0], muenzausgabe[0], muenzausgabe[0]);
            	}
            	else if((lZifferanzahl[lErsteMuenze] == 1) && (lZifferanzahl[lZweiteMuenze] == 2) && (lZifferanzahl[lDritteMuenze] == 2))
            	{
            		System.out.printf("%s%s%s%n%s%s%s%n%s%d%s%s%d%s%s%d%s%n%s%s%s%s%s%s%s%s%s%n%s%s%s%n%s%s%s\n", muenzausgabe[0], muenzausgabe[0], muenzausgabe[0], muenzausgabe[1], muenzausgabe[1], muenzausgabe[1], muenzausgabe[2], lMuenzwert[lErsteMuenze], muenzausgabe[3], muenzausgabe[2], lMuenzwert[lZweiteMuenze], muenzausgabe[6], muenzausgabe[2], lMuenzwert[lDritteMuenze], muenzausgabe[6], muenzausgabe[4], lEinheit[lErsteMuenze], muenzausgabe[5], muenzausgabe[4], lEinheit[lZweiteMuenze], muenzausgabe[5], muenzausgabe[4], lEinheit[lDritteMuenze], muenzausgabe[5], muenzausgabe[1], muenzausgabe[1], muenzausgabe[1], muenzausgabe[0], muenzausgabe[0], muenzausgabe[0]);
            	}
            	else if((lZifferanzahl[lErsteMuenze] == 2) && (lZifferanzahl[lZweiteMuenze] == 2) && (lZifferanzahl[lDritteMuenze] == 1))
            	{
            		System.out.printf("%s%s%s%n%s%s%s%n%s%d%s%s%d%s%s%d%s%n%s%s%s%s%s%s%s%s%s%n%s%s%s%n%s%s%s\n", muenzausgabe[0], muenzausgabe[0], muenzausgabe[0], muenzausgabe[1], muenzausgabe[1], muenzausgabe[1], muenzausgabe[2], lMuenzwert[lErsteMuenze], muenzausgabe[6], muenzausgabe[2], lMuenzwert[lZweiteMuenze], muenzausgabe[6], muenzausgabe[2], lMuenzwert[lDritteMuenze], muenzausgabe[3], muenzausgabe[4], lEinheit[lErsteMuenze], muenzausgabe[5], muenzausgabe[4], lEinheit[lZweiteMuenze], muenzausgabe[5], muenzausgabe[4], lEinheit[lDritteMuenze], muenzausgabe[5], muenzausgabe[1], muenzausgabe[1], muenzausgabe[1], muenzausgabe[0], muenzausgabe[0], muenzausgabe[0]);
            	}
            	else if((lZifferanzahl[lErsteMuenze] == 2) && (lZifferanzahl[lZweiteMuenze] == 2) && (lZifferanzahl[lDritteMuenze] == 2))
            	{
            		System.out.printf("%s%s%s%n%s%s%s%n%s%d%s%s%d%s%s%d%s%n%s%s%s%s%s%s%s%s%s%n%s%s%s%n%s%s%s\n", muenzausgabe[0], muenzausgabe[0], muenzausgabe[0], muenzausgabe[1], muenzausgabe[1], muenzausgabe[1], muenzausgabe[2], lMuenzwert[lErsteMuenze], muenzausgabe[6], muenzausgabe[2], lMuenzwert[lZweiteMuenze], muenzausgabe[6], muenzausgabe[2], lMuenzwert[lDritteMuenze], muenzausgabe[6], muenzausgabe[4], lEinheit[lErsteMuenze], muenzausgabe[5], muenzausgabe[4], lEinheit[lZweiteMuenze], muenzausgabe[5], muenzausgabe[4], lEinheit[lDritteMuenze], muenzausgabe[5], muenzausgabe[1], muenzausgabe[1], muenzausgabe[1], muenzausgabe[0], muenzausgabe[0], muenzausgabe[0]);
            	}
            }
            if(lMuenzenGesamt == 4)
            {
            	for(int l = 0; l < lAnzahlMuenzen.length; l++)
            	{
            		if(lAnzahlMuenzen[l] != 0)
            		{
            			if(lZifferanzahl[l] == 1)
            			{
            				System.out.printf("%s%n%s%n%s%d%s%n%s%s%s%n%s%n%s\n", muenzausgabe[0], muenzausgabe[1], muenzausgabe[2], lMuenzwert[l], muenzausgabe[3], muenzausgabe[4], lEinheit[l], muenzausgabe[5], muenzausgabe[1], muenzausgabe[0]);
            			}
            			else
            			{
            				System.out.printf("%s%n%s%n%s%d%s%n%s%s%s%n%s%n%s\n", muenzausgabe[0], muenzausgabe[1], muenzausgabe[2], lMuenzwert[l], muenzausgabe[6], muenzausgabe[4], lEinheit[l], muenzausgabe[5], muenzausgabe[1], muenzausgabe[0]);
            			}
            		}
            	}
            }
            if(lMuenzenGesamt == 5)
            {
            	for(int l = 0, m = 0; l < lAnzahlMuenzen.length; l++, m++)
            	{
            		if(lAnzahlMuenzen[l] != 0)
            		{
            			if(lVierteMuenze == 0)
            			{
            				lVierteMuenze = l;
            				lAnzahlMuenzen[l]--;
            			}
            		}
            		if(lAnzahlMuenzen[m] != 0)
            		{
            			if(lFuenfteMuenze == 0)
            			{
            				lFuenfteMuenze = m;
            				lAnzahlMuenzen[m]--;
            			}
            		}
            	}
            	if((lZifferanzahl[lVierteMuenze] == 1) && (lZifferanzahl[lFuenfteMuenze] == 1))
            	{
            		System.out.printf("%s%s%n%s%s%n%s%d%s%s%d%s%n%s%s%s%s%s%s%n%s%s%n%s%s\n", muenzausgabe[0], muenzausgabe[0], muenzausgabe[1], muenzausgabe[1], muenzausgabe[2], lMuenzwert[lVierteMuenze], muenzausgabe[3], muenzausgabe[2], lMuenzwert[lFuenfteMuenze], muenzausgabe[3], muenzausgabe[4], lEinheit[lVierteMuenze], muenzausgabe[5], muenzausgabe[4], lEinheit[lFuenfteMuenze], muenzausgabe[5], muenzausgabe[1], muenzausgabe[1], muenzausgabe[0], muenzausgabe[0]);
            	}
            	else if((lZifferanzahl[lVierteMuenze] == 1) && (lZifferanzahl[lFuenfteMuenze] == 2))
            	{
            		System.out.printf("%s%s%n%s%s%n%s%d%s%s%d%s%n%s%s%s%s%s%s%n%s%s%n%s%s\n", muenzausgabe[0], muenzausgabe[0], muenzausgabe[1], muenzausgabe[1], muenzausgabe[2], lMuenzwert[lVierteMuenze], muenzausgabe[3], muenzausgabe[2], lMuenzwert[lFuenfteMuenze], muenzausgabe[6], muenzausgabe[4], lEinheit[lVierteMuenze], muenzausgabe[5], muenzausgabe[4], lEinheit[lFuenfteMuenze], muenzausgabe[5], muenzausgabe[1], muenzausgabe[1], muenzausgabe[0], muenzausgabe[0]);
            	}
            	else if((lZifferanzahl[lVierteMuenze] == 2) && (lZifferanzahl[lFuenfteMuenze] == 1))
            	{
            		System.out.printf("%s%s%n%s%s%n%s%d%s%s%d%s%n%s%s%s%s%s%s%n%s%s%n%s%s\n", muenzausgabe[0], muenzausgabe[0], muenzausgabe[1], muenzausgabe[1], muenzausgabe[2], lMuenzwert[lVierteMuenze], muenzausgabe[6], muenzausgabe[2], lMuenzwert[lFuenfteMuenze], muenzausgabe[3], muenzausgabe[4], lEinheit[lVierteMuenze], muenzausgabe[5], muenzausgabe[4], lEinheit[lFuenfteMuenze], muenzausgabe[5], muenzausgabe[1], muenzausgabe[1], muenzausgabe[0], muenzausgabe[0]);
            	}
            	else if((lZifferanzahl[lVierteMuenze] == 2) && (lZifferanzahl[lFuenfteMuenze] == 2))
            	{
            		System.out.printf("%s%s%n%s%s%n%s%d%s%s%d%s%n%s%s%s%s%s%s%n%s%s%n%s%s\n", muenzausgabe[0], muenzausgabe[0], muenzausgabe[1], muenzausgabe[1], muenzausgabe[2], lMuenzwert[lVierteMuenze], muenzausgabe[6], muenzausgabe[2], lMuenzwert[lFuenfteMuenze], muenzausgabe[6], muenzausgabe[4], lEinheit[lVierteMuenze], muenzausgabe[5], muenzausgabe[4], lEinheit[lFuenfteMuenze], muenzausgabe[5], muenzausgabe[1], muenzausgabe[1], muenzausgabe[0], muenzausgabe[0]);
            	}
            }
            if(lMuenzenGesamt == 6)
            {
            	for(int l = 0, m = 0, n = 0; l < lAnzahlMuenzen.length; l++, m++, n++)
            	{
            		if(lAnzahlMuenzen[l] != 0)
            		{
            			if(lVierteMuenze == 0)
            			{
            				lVierteMuenze = l;
            				lAnzahlMuenzen[l]--;
            			}
            		}
            		if(lAnzahlMuenzen[m] != 0)
            		{
            			if(lFuenfteMuenze == 0)
            			{
            				lFuenfteMuenze = m;
            				lAnzahlMuenzen[m]--;
            			}
            		}
            		if(lAnzahlMuenzen[n] != 0)
            		{
            			if(lSechsteMuenze == 0)
            			{
            				lSechsteMuenze = n;
            				lAnzahlMuenzen[n]--;
            			}
            		}
            	}
            	if((lZifferanzahl[lVierteMuenze] == 1) && (lZifferanzahl[lFuenfteMuenze] == 1) && (lZifferanzahl[lSechsteMuenze] == 1))
            	{
            		System.out.printf("%s%s%s%n%s%s%s%n%s%d%s%s%d%s%s%d%s%n%s%s%s%s%s%s%s%s%s%n%s%s%s%n%s%s%s\n", muenzausgabe[0], muenzausgabe[0], muenzausgabe[0], muenzausgabe[1], muenzausgabe[1], muenzausgabe[1], muenzausgabe[2], lMuenzwert[lVierteMuenze], muenzausgabe[3], muenzausgabe[2], lMuenzwert[lFuenfteMuenze], muenzausgabe[3], muenzausgabe[2], lMuenzwert[lSechsteMuenze], muenzausgabe[3], muenzausgabe[4], lEinheit[lVierteMuenze], muenzausgabe[5], muenzausgabe[4], lEinheit[lFuenfteMuenze], muenzausgabe[5], muenzausgabe[4], lEinheit[lSechsteMuenze], muenzausgabe[5], muenzausgabe[1], muenzausgabe[1], muenzausgabe[1], muenzausgabe[0], muenzausgabe[0], muenzausgabe[0]);
            	}
            	else if((lZifferanzahl[lVierteMuenze] == 1) && (lZifferanzahl[lFuenfteMuenze] == 1) && (lZifferanzahl[lSechsteMuenze] == 2))
            	{
            		System.out.printf("%s%s%s%n%s%s%s%n%s%d%s%s%d%s%s%d%s%n%s%s%s%s%s%s%s%s%s%n%s%s%s%n%s%s%s\n", muenzausgabe[0], muenzausgabe[0], muenzausgabe[0], muenzausgabe[1], muenzausgabe[1], muenzausgabe[1], muenzausgabe[2], lMuenzwert[lVierteMuenze], muenzausgabe[3], muenzausgabe[2], lMuenzwert[lFuenfteMuenze], muenzausgabe[3], muenzausgabe[2], lMuenzwert[lSechsteMuenze], muenzausgabe[6], muenzausgabe[4], lEinheit[lVierteMuenze], muenzausgabe[5], muenzausgabe[4], lEinheit[lFuenfteMuenze], muenzausgabe[5], muenzausgabe[4], lEinheit[lSechsteMuenze], muenzausgabe[5], muenzausgabe[1], muenzausgabe[1], muenzausgabe[1], muenzausgabe[0], muenzausgabe[0], muenzausgabe[0]);
            	}
            	else if((lZifferanzahl[lVierteMuenze] == 1) && (lZifferanzahl[lFuenfteMuenze] == 2) && (lZifferanzahl[lSechsteMuenze] == 1))
            	{
            		System.out.printf("%s%s%s%n%s%s%s%n%s%d%s%s%d%s%s%d%s%n%s%s%s%s%s%s%s%s%s%n%s%s%s%n%s%s%s\n", muenzausgabe[0], muenzausgabe[0], muenzausgabe[0], muenzausgabe[1], muenzausgabe[1], muenzausgabe[1], muenzausgabe[2], lMuenzwert[lVierteMuenze], muenzausgabe[3], muenzausgabe[2], lMuenzwert[lFuenfteMuenze], muenzausgabe[6], muenzausgabe[2], lMuenzwert[lSechsteMuenze], muenzausgabe[3], muenzausgabe[4], lEinheit[lVierteMuenze], muenzausgabe[5], muenzausgabe[4], lEinheit[lFuenfteMuenze], muenzausgabe[5], muenzausgabe[4], lEinheit[lSechsteMuenze], muenzausgabe[5], muenzausgabe[1], muenzausgabe[1], muenzausgabe[1], muenzausgabe[0], muenzausgabe[0], muenzausgabe[0]);
            	}
            	else if((lZifferanzahl[lVierteMuenze] == 2) && (lZifferanzahl[lFuenfteMuenze] == 1) && (lZifferanzahl[lSechsteMuenze] == 1))
            	{
            		System.out.printf("%s%s%s%n%s%s%s%n%s%d%s%s%d%s%s%d%s%n%s%s%s%s%s%s%s%s%s%n%s%s%s%n%s%s%s\n", muenzausgabe[0], muenzausgabe[0], muenzausgabe[0], muenzausgabe[1], muenzausgabe[1], muenzausgabe[1], muenzausgabe[2], lMuenzwert[lVierteMuenze], muenzausgabe[6], muenzausgabe[2], lMuenzwert[lFuenfteMuenze], muenzausgabe[3], muenzausgabe[2], lMuenzwert[lSechsteMuenze], muenzausgabe[3], muenzausgabe[4], lEinheit[lVierteMuenze], muenzausgabe[5], muenzausgabe[4], lEinheit[lFuenfteMuenze], muenzausgabe[5], muenzausgabe[4], lEinheit[lSechsteMuenze], muenzausgabe[5], muenzausgabe[1], muenzausgabe[1], muenzausgabe[1], muenzausgabe[0], muenzausgabe[0], muenzausgabe[0]);
            	}
            	else if((lZifferanzahl[lVierteMuenze] == 2) && (lZifferanzahl[lFuenfteMuenze] == 2) && (lZifferanzahl[lSechsteMuenze] == 1))
            	{
            		System.out.printf("%s%s%s%n%s%s%s%n%s%d%s%s%d%s%s%d%s%n%s%s%s%s%s%s%s%s%s%n%s%s%s%n%s%s%s\n", muenzausgabe[0], muenzausgabe[0], muenzausgabe[0], muenzausgabe[1], muenzausgabe[1], muenzausgabe[1], muenzausgabe[2], lMuenzwert[lVierteMuenze], muenzausgabe[6], muenzausgabe[2], lMuenzwert[lFuenfteMuenze], muenzausgabe[6], muenzausgabe[2], lMuenzwert[lSechsteMuenze], muenzausgabe[3], muenzausgabe[4], lEinheit[lVierteMuenze], muenzausgabe[5], muenzausgabe[4], lEinheit[lFuenfteMuenze], muenzausgabe[5], muenzausgabe[4], lEinheit[lSechsteMuenze], muenzausgabe[5], muenzausgabe[1], muenzausgabe[1], muenzausgabe[1], muenzausgabe[0], muenzausgabe[0], muenzausgabe[0]);
            	}
            	else if((lZifferanzahl[lVierteMuenze] == 1) && (lZifferanzahl[lFuenfteMuenze] == 2) && (lZifferanzahl[lSechsteMuenze] == 2))
            	{
            		System.out.printf("%s%s%s%n%s%s%s%n%s%d%s%s%d%s%s%d%s%n%s%s%s%s%s%s%s%s%s%n%s%s%s%n%s%s%s\n", muenzausgabe[0], muenzausgabe[0], muenzausgabe[0], muenzausgabe[1], muenzausgabe[1], muenzausgabe[1], muenzausgabe[2], lMuenzwert[lVierteMuenze], muenzausgabe[3], muenzausgabe[2], lMuenzwert[lFuenfteMuenze], muenzausgabe[6], muenzausgabe[2], lMuenzwert[lSechsteMuenze], muenzausgabe[6], muenzausgabe[4], lEinheit[lVierteMuenze], muenzausgabe[5], muenzausgabe[4], lEinheit[lFuenfteMuenze], muenzausgabe[5], muenzausgabe[4], lEinheit[lSechsteMuenze], muenzausgabe[5], muenzausgabe[1], muenzausgabe[1], muenzausgabe[1], muenzausgabe[0], muenzausgabe[0], muenzausgabe[0]);
            	}
            	else if((lZifferanzahl[lVierteMuenze] == 2) && (lZifferanzahl[lFuenfteMuenze] == 2) && (lZifferanzahl[lSechsteMuenze] == 1))
            	{
            		System.out.printf("%s%s%s%n%s%s%s%n%s%d%s%s%d%s%s%d%s%n%s%s%s%s%s%s%s%s%s%n%s%s%s%n%s%s%s\n", muenzausgabe[0], muenzausgabe[0], muenzausgabe[0], muenzausgabe[1], muenzausgabe[1], muenzausgabe[1], muenzausgabe[2], lMuenzwert[lVierteMuenze], muenzausgabe[6], muenzausgabe[2], lMuenzwert[lFuenfteMuenze], muenzausgabe[6], muenzausgabe[2], lMuenzwert[lSechsteMuenze], muenzausgabe[3], muenzausgabe[4], lEinheit[lVierteMuenze], muenzausgabe[5], muenzausgabe[4], lEinheit[lFuenfteMuenze], muenzausgabe[5], muenzausgabe[4], lEinheit[lSechsteMuenze], muenzausgabe[5], muenzausgabe[1], muenzausgabe[1], muenzausgabe[1], muenzausgabe[0], muenzausgabe[0], muenzausgabe[0]);
            	}
            	else if((lZifferanzahl[lVierteMuenze] == 2) && (lZifferanzahl[lFuenfteMuenze] == 2) && (lZifferanzahl[lSechsteMuenze] == 2))
            	{
            		System.out.printf("%s%s%s%n%s%s%s%n%s%d%s%s%d%s%s%d%s%n%s%s%s%s%s%s%s%s%s%n%s%s%s%n%s%s%s\n", muenzausgabe[0], muenzausgabe[0], muenzausgabe[0], muenzausgabe[1], muenzausgabe[1], muenzausgabe[1], muenzausgabe[2], lMuenzwert[lVierteMuenze], muenzausgabe[6], muenzausgabe[2], lMuenzwert[lFuenfteMuenze], muenzausgabe[6], muenzausgabe[2], lMuenzwert[lSechsteMuenze], muenzausgabe[6], muenzausgabe[4], lEinheit[lVierteMuenze], muenzausgabe[5], muenzausgabe[4], lEinheit[lFuenfteMuenze], muenzausgabe[5], muenzausgabe[4], lEinheit[lSechsteMuenze], muenzausgabe[5], muenzausgabe[1], muenzausgabe[1], muenzausgabe[1], muenzausgabe[0], muenzausgabe[0], muenzausgabe[0]);
            	}
            }
        }
    	betrag = Math.round(betrag*1000)/1000.0;
    	if((betrag <= 0.04) && (betrag > 0.001)) 																	//Restgeld unter 5 Cent übrig? bzw ob nicht-annehmbares Kleingeld von unter 5 Cent eingezahlt worden ist
        {
    		System.out.println("\nFolgende Beträge konnten durch den Automaten nicht angenommen werden:\n");
    		betrag = Math.round(betrag*1000)/1000.0;
    		if(betrag == 0.04)																						//wie viel genau? -> anschließend Ausgabe
    		{
    			System.out.printf("%s%s%n%s%s%n%s%d%s%s%d%s%n%s%s%s%s%s%s%n%s%s%n%s%s\n", muenzausgabe[0], muenzausgabe[0], muenzausgabe[1], muenzausgabe[1], muenzausgabe[2], 2, muenzausgabe[3], muenzausgabe[2], 2, muenzausgabe[3], muenzausgabe[4], "CENT", muenzausgabe[5], muenzausgabe[4], "CENT", muenzausgabe[5], muenzausgabe[1], muenzausgabe[1], muenzausgabe[0], muenzausgabe[0]);
    		}
    		else if(betrag == 0.03)
    		{
    			System.out.printf("%s%s%n%s%s%n%s%d%s%s%d%s%n%s%s%s%s%s%s%n%s%s%n%s%s\n", muenzausgabe[0], muenzausgabe[0], muenzausgabe[1], muenzausgabe[1], muenzausgabe[2], 2, muenzausgabe[3], muenzausgabe[2], 1, muenzausgabe[3], muenzausgabe[4], "CENT", muenzausgabe[5], muenzausgabe[4], "CENT", muenzausgabe[5], muenzausgabe[1], muenzausgabe[1], muenzausgabe[0], muenzausgabe[0]);
    		}
    		else if(betrag == 0.02)
    		{
    			System.out.printf("%s%n%s%n%s%d%s%n%s%s%s%n%s%n%s\n", muenzausgabe[0], muenzausgabe[1], muenzausgabe[2], 2, muenzausgabe[3], muenzausgabe[4], "CENT", muenzausgabe[5], muenzausgabe[1], muenzausgabe[0]);
    		}
    		else if(betrag == 0.01)
    		{
    			System.out.printf("%s%n%s%n%s%d%s%n%s%s%s%n%s%n%s\n", muenzausgabe[0], muenzausgabe[1], muenzausgabe[2], 1, muenzausgabe[3], muenzausgabe[4], "CENT", muenzausgabe[5], muenzausgabe[1], muenzausgabe[0]);
    		}
        }
   		System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
   						   "vor Fahrtantritt entwerten zu lassen!\n"+
		   		   		   "Wir wünschen Ihnen eine gute Fahrt.");
    }
    
    // Fahrscheinausgabe
    // -----------------
    public static void fahrscheinausgabe(long millisekunde)
    {
    	System.out.println("\nFahrschein wird ausgegeben");															//Fahrscheinausgabe
        for (int i = 0; i < 8; i++)
        {
           warte(millisekunde);
        }
    }
    
    // Wartezeit
    // ---------
    public static void warte(long millisekunde)
    {
    	System.out.print("=");
        try 
        {
        Thread.sleep(millisekunde);
        } 
        catch (InterruptedException e) 
        {
        e.printStackTrace();
        }
    }
}