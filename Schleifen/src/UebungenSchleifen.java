public class UebungenSchleifen {

	public static void main(String[] args) {
	
		//Aufgabe 5
		int zahl = 1;
		int multiplikator = 1;
		boolean repeat = true;
		
		while(repeat == true)
		{
			for(int i = 0; i < 10; i++)
			{
				System.out.printf("%d%s%d%s", zahl, "x", multiplikator, " ");
				multiplikator++;
			}
			System.out.println();
			multiplikator = 1;
			zahl++;
			if(zahl > 10)
			{
				repeat = false;
			}
		}
	}
}
