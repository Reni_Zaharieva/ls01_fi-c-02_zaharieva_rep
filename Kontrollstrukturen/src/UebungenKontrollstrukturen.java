import java.io.PrintStream;
import java.util.Scanner;

public class UebungenKontrollstrukturen {

	//Aufgabe 1
	static int zahlMonat = 11;
	//Aufgabe 2
	static double nettoPreis;
	static char ermaessigung;
	//Aufgabe 3
	static int schulnote;
	//Aufgabe 4
	static int bestellmenge;
	static double einzelpreis;
	//Aufgabe 5
	static double koerpergewicht;
	static double koerpergroesse;
	static char geschlecht;
	
	public static void main(String[] args)
	{
		Scanner tastatur = new Scanner(System.in);
		
		//Aufgabe 1
		System.out.println("Aufgabe 1:");
		System.out.println("Der gew�hlte Monat ist: " + monatIf(zahlMonat));
		System.out.println("Der gew�hlte Monat ist: " + monatCase(zahlMonat));
		//Aufgabe 2
		System.out.printf("%n%s", "Aufgabe 2:");
		System.out.printf("%n%s", "Bitte geben Sie einen Nettopreis ein: ");
		nettoPreis = tastatur.nextDouble();
		System.out.printf("%s", "Geben Sie 'j' f�r eine erm��igte oder 'n' f�r eine volle Versteuerung ein: ");
		ermaessigung = tastatur.next().charAt(0);
		System.out.printf("%s%.2f%s%n%n", "Der versteuerte Bruttopreis betr�gt: ", steuer(nettoPreis, ermaessigung), "�");
		//Aufgabe 3
		System.out.printf("%s%n", "Aufgabe 3:");
		System.out.println("Bite geben Sie eine Zahl zwischen 1 und 6 ein: ");
		schulnote = tastatur.nextInt();
		System.out.printf("%s%n", ermittleSchulnote(schulnote));
		//Aufgabe 4
		System.out.printf("%n%s", "Aufgabe 4:");
		System.out.printf("%n%s", "Geben Sie die gew�nschte Bestellmenge ein: ");
		bestellmenge = tastatur.nextInt();
		System.out.printf("%s", "Bitte geben Sie den Einzelpreis ein: ");
		einzelpreis = tastatur.nextDouble();
		System.out.printf("%s%.2f%s", "Die Bestellung kostet ", rechnung(bestellmenge, einzelpreis), "� inkl. MwSt.");
		//Aufgabe 5
		System.out.printf("%n%n%s", "Aufgabe 5:");
		System.out.printf("%n%s", "Geben Sie Ihr Geschlecht an (m/w): ");
		do
		{
			System.out.println("Bitte geben Sie 'm' oder 'w' ein. ");
			geschlecht = tastatur.next().charAt(0);
		}
		while((geschlecht != 'm') && (geschlecht != 'w'));
		System.out.println("Geben Sie bitte Ihre K�rpergr��e ein: ");
		koerpergroesse = tastatur.nextDouble();
		System.out.println("Geben Sie bitte Ihr K�rpergewicht ein: ");
		koerpergewicht = tastatur.nextDouble();
		System.out.printf("%n%s", bmi(koerpergroesse, koerpergewicht, geschlecht));
		
		tastatur.close();
	}
	
	//Aufgabe 1.a)
	public static String monatIf(int zahlMonat)
	{
		if(zahlMonat == 1)
		{
			return "Januar";
		}
		else if(zahlMonat == 2)
		{
			return "Februar";
		}
		else if(zahlMonat == 3)
		{
			return "M�rz";
		}
		else if(zahlMonat == 4)
		{
			return "April";
		}
		else if(zahlMonat == 5)
		{
			return "Mai";
		}
		else if(zahlMonat == 6)
		{
			return "Juni";
		}
		else if(zahlMonat == 7)
		{
			return "Juli";
		}
		else if(zahlMonat == 8)
		{
			return "August";
		}
		else if(zahlMonat == 9)
		{
			return "September";
		}
		else if(zahlMonat == 10)
		{
			return "Oktober";
		}
		else if(zahlMonat == 11)
		{
			return "November";
		}
		else if(zahlMonat == 12)
		{
			return "Dezember";
		}
		else
		{
			return "Die Zahl muss einen Wert zwischen 1 und 12 haben.";
		}
	}
	
	//Aufgabe 1.b)
	public static String monatCase(int zahlMonat)
	{
		switch (zahlMonat)
		{
			case 1: 
			{
				return "Januar";
			}
			case 2:
			{
				return "Februar";
			}
			case 3:
			{
				return "M�rz";
			}
			case 4:
			{
				return "April";
			}
			case 5:
			{
				return "Mai";
			}
			case 6:
			{
				return "Juni";
			}
			case 7:
			{
				return "Juli";
			}
			case 8:
			{
				return "August";
			}
			case 9:
			{
				return "September";
			}
			case 10:
			{
				return "Oktober";
			}
			case 11:
			{
				return "November";
			}
			case 12:
			{
				return "Dezember";
			}
			default:
			{
				return "Die Zahl muss einen Wert zwischen 1 und 12 haben.";
			}
		}
	}
	
	//Aufgabe 2
	public static double steuer(double nettoPreis, char ermaessigung)
	{
		int lProzentsatz;
		double lBruttoPreis;
		
		if(ermaessigung == 'j')
		{
			lProzentsatz = 7;
			lBruttoPreis = nettoPreis + ((nettoPreis * lProzentsatz)/100);

			return lBruttoPreis;
		}
		else if(ermaessigung == 'n')
		{
			lProzentsatz = 19;
			lBruttoPreis = nettoPreis + ((nettoPreis * lProzentsatz)/100);
			
			return lBruttoPreis;
		}
		else
		{
			System.out.println("Fehler. Bitte starten Sie das Programm neu und geben 'j' oder 'n' an.");	
			return 0000.0000;
		}
	}
	
	//Aufgabe 3
	public static String ermittleSchulnote(int schulnote)
	{		
		if((schulnote >= 1) && (schulnote <= 6))
		{
			switch(schulnote)
			{
				case 1:
				{
					return "Der angegebene Wert entspricht der Schulnote: Sehr gut";
				}
				case 2:
				{
					return "Der angegebene Wert entspricht der Schulnote: Gut";
				}
				case 3:
				{
					return "Der angegebene Wert entspricht der Schulnote: Befriedigend";
				}
				case 4:
				{
					return "Der angegebene Wert entspricht der Schulnote: Ausreichend";
				}
				case 5:
				{
					return "Der angegebene Wert entspricht der Schulnote: Mangelhaft";
				}
				case 6:
				{
					return "Der angegebene Wert entspricht der Schulnote: Ungen�gend";
				}
				default:
				{
					return "Fehler: Der angegebene Wert entspricht keiner g�ltigen Schulnote.";
				}
			}
		}
		else
		{
			return "Fehler: Der angegebene Wert entspricht keiner g�ltigen Schulnote.";
		}
	}
	
	//Aufgabe 4
	public static double rechnung(int bestellmenge, double einzelpreis)
	{
		double lGesamtpreis;
		
		if(bestellmenge < 10)
		{
			lGesamtpreis = (bestellmenge*einzelpreis)+((bestellmenge*einzelpreis)*19)/100;
			return lGesamtpreis;
		}
		else if(bestellmenge >= 10)
		{
			lGesamtpreis = ((bestellmenge*einzelpreis)+((bestellmenge*einzelpreis)*19)/100) + 10;
			return lGesamtpreis;
		}
		else
		{
			System.out.println("Ihre Eingabe ist fehlerhaft. Starten Sie das Programm neu und geben eine valide Bestellmenge und Einzelpreis ein");
			return 0000.0000;
		}
	}
	
	//Aufgabe 5
	public static String bmi(double koerpergroesse, double koerpergewicht, char geschlecht)
	{
		double lBmi;
		String lKlassifikation;
		
		koerpergroesse = ((koerpergroesse/100)*(koerpergroesse/100));
		lBmi = koerpergewicht/koerpergroesse;
		
		if(((lBmi < 20) && (geschlecht == 'm')) || ((lBmi < 19) && (geschlecht == 'w')))
		{
			lKlassifikation = "Untergewicht";
		}
		else if(((lBmi > 20) && (lBmi < 25) && (geschlecht == 'm') || (lBmi > 19) && (lBmi < 24) && (geschlecht == 'w')))
		{
			lKlassifikation = "Normalgewicht";
		}
		else
		{
			lKlassifikation = "�bergewicht";
		}
		
		return String.format("%s%.1f%s%n%s%s%s", "Ihr BMI betr�gt einen Wert von ", lBmi, ".", "Somit befinden Sie sich im Bereich des ", lKlassifikation, "s.");
	}
}
